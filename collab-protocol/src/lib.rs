//! Types for the collab protocol.

use {
    core::{
        operation::{FileId, RevisionId},
        path::PathBuf,
        Delta, Operation, Uuid,
    },
    std::{collections::HashMap, error, fmt},
};

/// A request from a client to the server.
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Request {
    pub id: RequestResponseId,
    pub body: RequestBody,
}

/// A response from the server to a client.
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Response {
    pub id: RequestResponseId,
    pub body: ResponseBody,
}

/// A unique identifier used to match a response with a request.
pub type RequestResponseId = usize;

/// The body of a request message.
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum RequestBody {
    CreateProject(),
    JoinProject(ProjectId),
    LeaveProject(ProjectId),
    CreateFile(ProjectId, PathBuf),
    DeleteFile(ProjectId, PathBuf),
    UpdateFile(ProjectId, FileId, RevisionId, Option<Delta>),
}

/// The body of a response message.
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum ResponseBody {
    CreateProject(ProjectId),
    JoinProject(Result<ProjectSnapshot, Error>),
    LeaveProject(Result<(), Error>),
    CreateFile(Result<(), Error>),
    DeleteFile(Result<(), Error>),
    UpdateFile(Result<(), Error>),
}

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct ProjectSnapshot {
    pub files_by_id: HashMap<FileId, FileSnapshot>,
    pub file_ids_by_path: HashMap<PathBuf, FileId>,
    pub committed_operations: Vec<Operation>,
}

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct FileSnapshot {
    pub outstanding_deltas: Vec<Delta>,
}

/// An error while handling a request
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Error {
    /// The client is already a member of a project.
    AlreadyAMember,
    /// A project was not found.
    ProjectNotFound,
    /// The client is not a member of a project.
    NotAMember,
    /// A file already exists.
    FileAlreadyExists,
    /// A file was not found.
    FileNotFound,
    /// A revision was not found
    RevisionNotFound,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::AlreadyAMember => write!(f, "already a member"),
            Error::ProjectNotFound => write!(f, "project not found"),
            Error::NotAMember => write!(f, "not a member"),
            Error::FileAlreadyExists => write!(f, "file already exists"),
            Error::FileNotFound => write!(f, "file not found"),
            Error::RevisionNotFound => write!(f, "revision not found"),
        }
    }
}

impl error::Error for Error {}

/// A notification from the server to a client.
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Notification {
    OperationCommitted(ProjectId, Operation),
    FileUpdated(ProjectId, FileId, Delta),
}

/// A globally unique identifier for a project.
pub type ProjectId = Uuid;
