use {
    collab_protocol::{
        Error, Notification, ProjectSnapshot, Request, RequestBody, Response, ResponseBody,
    },
    collab_server::Server,
    core::{path::Path, Operation},
    std::{collections::HashMap, sync::mpsc},
};

#[test]
fn test_create_file() {
    // Create a collab server.
    let server = Server::new();

    // Create a notification channel and handler for the first client.
    let (notification_sender_0, notification_receiver_0) = mpsc::channel();
    let mut handler_0 = server.accept(notification_sender_0);

    // Create a notification channel and handler for the second client.
    let (notification_sender_1, notification_receiver_1) = mpsc::channel();
    let mut handler_1 = server.accept(notification_sender_1);

    // Let the first client create a new project.
    let response = handler_0.handle_request(Request {
        id: 0,
        body: RequestBody::CreateProject(),
    });
    let project_id = match response.body {
        ResponseBody::CreateProject(project_id) => project_id,
        _ => panic!(),
    };

    // Let the second client join the new project.
    assert_eq!(
        handler_1.handle_request(Request {
            id: 0,
            body: RequestBody::JoinProject(project_id)
        }),
        Response {
            id: 0,
            body: ResponseBody::JoinProject(Ok(ProjectSnapshot {
                files_by_id: HashMap::new(),
                file_ids_by_path: HashMap::new(),
                committed_operations: Vec::new(),
            })),
        }
    );

    // Let the first client create a new file. This should cause a notification to be sent to the
    // second client, but not the first client.
    let path = Path::from_str("main.rs").unwrap().to_owned();
    assert_eq!(
        handler_0.handle_request(Request {
            id: 1,
            body: RequestBody::CreateFile(project_id, path.clone()),
        }),
        Response {
            id: 1,
            body: ResponseBody::CreateFile(Ok(()))
        }
    );
    assert!(notification_receiver_0.try_recv().is_err());
    assert_eq!(
        *notification_receiver_1.try_recv().unwrap(),
        Notification::OperationCommitted(project_id, Operation::CreateFile(path.clone()))
    );

    // Let the second client create a new file with the same name. This should result in an error,
    // since the file already exists.
    assert_eq!(
        handler_1.handle_request(Request {
            id: 1,
            body: RequestBody::CreateFile(project_id, path),
        }),
        Response {
            id: 1,
            body: ResponseBody::CreateFile(Err(Error::FileAlreadyExists))
        }
    );
}
