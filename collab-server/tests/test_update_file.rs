use {
    collab_protocol::{
        Notification, ProjectSnapshot, Request, RequestBody, Response, ResponseBody,
    },
    collab_server::Server,
    core::{path::Path, Delta, Operation, Position, Text},
    std::{collections::HashMap, sync::mpsc},
};

#[test]
fn test_update_file() {
    // Create a collab server.
    let server = Server::new();

    // Create a notification channel and handler for the first client.
    let (notification_sender_0, notification_receiver_0) = mpsc::channel();
    let mut handler_0 = server.accept(notification_sender_0);

    // Create a notification channel and handler for the second client.
    let (notification_sender_1, notification_receiver_1) = mpsc::channel();
    let mut handler_1 = server.accept(notification_sender_1);

    // Let the first client create a new project.
    let response = handler_0.handle_request(Request {
        id: 0,
        body: RequestBody::CreateProject(),
    });
    let project_id = match response.body {
        ResponseBody::CreateProject(project_id) => project_id,
        _ => panic!(),
    };

    // Let the second client join the new project.
    assert_eq!(
        handler_1.handle_request(Request {
            id: 0,
            body: RequestBody::JoinProject(project_id)
        }),
        Response {
            id: 0,
            body: ResponseBody::JoinProject(Ok(ProjectSnapshot {
                files_by_id: HashMap::new(),
                file_ids_by_path: HashMap::new(),
                committed_operations: Vec::new(),
            }))
        }
    );

    // Let the first client create a new file.
    let path = Path::from_str("main.rs").unwrap().to_owned();
    assert_eq!(
        handler_0.handle_request(Request {
            id: 1,
            body: RequestBody::CreateFile(project_id, path.clone()),
        }),
        Response {
            id: 1,
            body: ResponseBody::CreateFile(Ok(()))
        }
    );
    // Receive all notifications.
    notification_receiver_1.try_recv().unwrap();

    // Let the first and second client concurrently insert the string "abc" and "def" at the start
    // of the file, respectively, such that the delta from the first client arrives at the server
    // before the one from the second client. This should cause a notification to be sent to both
    // clients.
    //
    // Since the delta from the first client arrived at the server before the one from the second
    // client, the delta from the second client should be transformed against the one from the first
    // client.
    assert_eq!(
        handler_0.handle_request(Request {
            id: 2,
            body: RequestBody::UpdateFile(
                project_id,
                0,
                0,
                Some(Delta::insert(
                    Position::start(),
                    Text::from_lines(vec![vec!['a', 'b', 'c']]).unwrap()
                ))
            ),
        }),
        Response {
            id: 2,
            body: ResponseBody::UpdateFile(Ok(()))
        }
    );
    assert_eq!(
        handler_1.handle_request(Request {
            id: 1,
            body: RequestBody::UpdateFile(
                project_id,
                0,
                0,
                Some(Delta::insert(
                    Position::start(),
                    Text::from_lines(vec![vec!['d', 'e', 'f']]).unwrap()
                ))
            ),
        }),
        Response {
            id: 1,
            body: ResponseBody::UpdateFile(Ok(()))
        }
    );
    assert_eq!(
        *notification_receiver_0.try_recv().unwrap(),
        Notification::FileUpdated(
            project_id,
            0,
            Delta::insert(
                Position {
                    line_index: 0,
                    column_index: 3,
                },
                Text::from_lines(vec![vec!['d', 'e', 'f']]).unwrap()
            )
        )
    );
    assert_eq!(
        *notification_receiver_1.try_recv().unwrap(),
        Notification::FileUpdated(
            project_id,
            0,
            Delta::insert(
                Position::start(),
                Text::from_lines(vec![vec!['a', 'b', 'c']]).unwrap()
            )
        )
    );

    // Let the first client acknowledge that it has seen revision 1 from the server. This should
    // cause no notifications to be sent, since the second client has not yet acknowledged that it
    // has seen revision 1 from the server.
    assert_eq!(
        handler_0.handle_request(Request {
            id: 3,
            body: RequestBody::UpdateFile(project_id, 0, 1, None),
        }),
        Response {
            id: 3,
            body: ResponseBody::UpdateFile(Ok(()))
        }
    );
    assert!(notification_receiver_0.try_recv().is_err());
    assert!(notification_receiver_1.try_recv().is_err());

    // Let the second client acknowledge that it has seen revision 1 from the server. This should
    // cause a notification to be sent to both clients, since both clients have now acknowledged
    // that they have seen revision 1 from the server.
    assert_eq!(
        handler_1.handle_request(Request {
            id: 2,
            body: RequestBody::UpdateFile(project_id, 0, 1, None,),
        }),
        Response {
            id: 2,
            body: ResponseBody::UpdateFile(Ok(()))
        }
    );
    assert_eq!(
        *notification_receiver_0.try_recv().unwrap(),
        Notification::OperationCommitted(
            project_id,
            Operation::UpdateFile(
                0,
                Delta::insert(
                    Position::start(),
                    Text::from_lines(vec![vec!['a', 'b', 'c']]).unwrap()
                )
            )
        )
    );
    assert_eq!(
        *notification_receiver_1.try_recv().unwrap(),
        Notification::OperationCommitted(
            project_id,
            Operation::UpdateFile(
                0,
                Delta::insert(
                    Position::start(),
                    Text::from_lines(vec![vec!['a', 'b', 'c']]).unwrap()
                )
            )
        )
    );
}
