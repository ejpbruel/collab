use {
    collab_protocol::{Request, RequestBody, ResponseBody},
    collab_server::Server,
    std::sync::mpsc,
};

#[test]
fn test_create_project() {
    // Create a collab server.
    let server = Server::new();

    // Create a notification channel and handler for the client.
    let (notification_sender, _) = mpsc::channel();
    let mut handler = server.accept(notification_sender);

    // Let the client create a new project.
    let response = handler.handle_request(Request {
        id: 0,
        body: RequestBody::CreateProject(),
    });
    assert_eq!(response.id, 0);
    match response.body {
        ResponseBody::CreateProject(_) => {}
        _ => panic!(),
    }
}
