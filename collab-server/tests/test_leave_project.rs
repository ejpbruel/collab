use {
    collab_protocol::{Error, ProjectSnapshot, Request, RequestBody, Response, ResponseBody},
    collab_server::Server,
    std::{collections::HashMap, sync::mpsc},
};

#[test]
fn test_leave_project() {
    // Create a collab server.
    let server = Server::new();

    // Create a notification channel and handler for the first client.
    let (notification_sender_0, _) = mpsc::channel();
    let mut handler_0 = server.accept(notification_sender_0);

    // Create a notification channel and handler for the second client.
    let (notification_sender_1, _) = mpsc::channel();
    let mut handler_1 = server.accept(notification_sender_1);

    // Let the first client create a new project.
    let response = handler_0.handle_request(Request {
        id: 0,
        body: RequestBody::CreateProject(),
    });
    let project_id = match response.body {
        ResponseBody::CreateProject(project_id) => project_id,
        _ => panic!(),
    };

    // Let the second client join the new project.
    assert_eq!(
        handler_1.handle_request(Request {
            id: 0,
            body: RequestBody::JoinProject(project_id)
        }),
        Response {
            id: 0,
            body: ResponseBody::JoinProject(Ok(ProjectSnapshot {
                files_by_id: HashMap::new(),
                file_ids_by_path: HashMap::new(),
                committed_operations: Vec::new(),
            }))
        }
    );

    // Let the first client leave the project.
    assert_eq!(
        handler_0.handle_request(Request {
            id: 1,
            body: RequestBody::LeaveProject(project_id)
        }),
        Response {
            id: 1,
            body: ResponseBody::LeaveProject(Ok(()))
        }
    );

    // Let the second client leave the project.
    assert_eq!(
        handler_1.handle_request(Request {
            id: 1,
            body: RequestBody::LeaveProject(project_id)
        }),
        Response {
            id: 1,
            body: ResponseBody::LeaveProject(Ok(()))
        }
    );

    // Let the first client leave the project again. This should result in an error, since the
    // first client is not a member of the project.
    assert_eq!(
        handler_0.handle_request(Request {
            id: 2,
            body: RequestBody::LeaveProject(project_id)
        }),
        Response {
            id: 2,
            body: ResponseBody::LeaveProject(Err(Error::NotAMember))
        }
    );

    // Let the second client leave the project again. This should result in an error, since the
    // second client is not a member of the project.
    assert_eq!(
        handler_1.handle_request(Request {
            id: 2,
            body: RequestBody::LeaveProject(project_id)
        }),
        Response {
            id: 2,
            body: ResponseBody::LeaveProject(Err(Error::NotAMember))
        }
    );
}
