use {
    collab_protocol::{
        FileSnapshot, Notification, ProjectSnapshot, Request, RequestBody, Response, ResponseBody,
    },
    collab_server::Server,
    core::{path::Path, Delta, Operation, Position, Text},
    std::{collections::HashMap, sync::mpsc},
};

#[test]
fn test_snapshots() {
    // Create a collab server.
    let server = Server::new();

    // Create a notification channel and handler for the first client.
    let (notification_sender_0, notification_receiver_0) = mpsc::channel();
    let mut handler_0 = server.accept(notification_sender_0);

    // Create a notification channel and handler for the second client.
    let (notification_sender_1, _) = mpsc::channel();
    let mut handler_1 = server.accept(notification_sender_1);

    // Let the first client create a new project.
    let response = handler_0.handle_request(Request {
        id: 0,
        body: RequestBody::CreateProject(),
    });
    let project_id = match response.body {
        ResponseBody::CreateProject(project_id) => project_id,
        _ => panic!(),
    };

    // Let the first client create a new file.
    let path = Path::from_str("main.rs").unwrap().to_owned();
    assert_eq!(
        handler_0.handle_request(Request {
            id: 1,
            body: RequestBody::CreateFile(project_id, path.clone()),
        }),
        Response {
            id: 1,
            body: ResponseBody::CreateFile(Ok(()))
        }
    );

    // Let the first client insert the string "abc" at the start of the new file.
    assert_eq!(
        handler_0.handle_request(Request {
            id: 2,
            body: RequestBody::UpdateFile(
                project_id,
                0,
                0,
                Some(Delta::insert(
                    Position::start(),
                    Text::from_lines(vec![vec!['a', 'b', 'c']]).unwrap()
                ))
            ),
        }),
        Response {
            id: 2,
            body: ResponseBody::UpdateFile(Ok(()))
        }
    );

    // Let the first client insert the string "def" after the string "abc". This should cause a
    // notification to be sent to the first client, since inserting the string "def" after the
    // string "abc" implies that the first client has seen the string "abc", and thus revision 1
    // from the server.
    assert_eq!(
        handler_0.handle_request(Request {
            id: 2,
            body: RequestBody::UpdateFile(
                project_id,
                0,
                1,
                Some(Delta::insert(
                    Position {
                        line_index: 0,
                        column_index: 3,
                    },
                    Text::from_lines(vec![vec!['d', 'e', 'f']]).unwrap()
                ))
            ),
        }),
        Response {
            id: 2,
            body: ResponseBody::UpdateFile(Ok(()))
        }
    );
    assert_eq!(
        *notification_receiver_0.try_recv().unwrap(),
        Notification::OperationCommitted(
            project_id,
            Operation::UpdateFile(
                0,
                Delta::insert(
                    Position::start(),
                    Text::from_lines(vec![vec!['a', 'b', 'c']]).unwrap()
                )
            )
        )
    );

    // Let the second client join the new project. The resulting snapshot of the project should
    // contain the the following committed operations:
    // 1. A new file was created.
    // 2. The string "abc" was inserted at the start of the new file.
    //
    // The resulting snapshot for the new file should contain the following outstanding deltas:
    // 1. The string "def" was inserted at the end of the new file.
    assert_eq!(
        handler_1.handle_request(Request {
            id: 0,
            body: RequestBody::JoinProject(project_id)
        }),
        Response {
            id: 0,
            body: ResponseBody::JoinProject(Ok(ProjectSnapshot {
                files_by_id: [(
                    0,
                    FileSnapshot {
                        outstanding_deltas: vec![Delta::insert(
                            Position {
                                line_index: 0,
                                column_index: 3,
                            },
                            Text::from_lines(vec![vec!['d', 'e', 'f']]).unwrap()
                        )]
                    }
                )]
                .iter()
                .cloned()
                .collect::<HashMap<_, _>>(),
                file_ids_by_path: [(path.clone(), 0)]
                    .iter()
                    .cloned()
                    .collect::<HashMap<_, _>>(),
                committed_operations: vec![
                    Operation::CreateFile(path),
                    Operation::UpdateFile(
                        0,
                        Delta::insert(
                            Position::start(),
                            Text::from_lines(vec![vec!['a', 'b', 'c']]).unwrap()
                        )
                    )
                ],
            }))
        }
    );
}
