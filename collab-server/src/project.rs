use {
    collab_protocol::{Error, FileSnapshot, Notification, ProjectId, ProjectSnapshot},
    core::{
        operation::{FileId, RevisionId},
        path::PathBuf,
        Delta, Operation,
    },
    rand::{rngs::StdRng, Rng, SeedableRng},
    std::{
        collections::{HashMap, VecDeque},
        pin::Pin,
        ptr::NonNull,
        sync::{mpsc::Sender, Arc, Mutex},
    },
};

// A project registry maintains a shared, thread-safe project table. The projects are owned by the
// registry, but it can hand out cloneable handles to each project. For each project, the registry
// maintains a count of the number of handles to that project. When the number of handles to a
// project reaches zero, the project is dropped.
//
// The project table, as well as the handle count for each project, is protected by a single mutex.
// Creating a new project, obtaining a handle to an existing project, or cloning or dropping an
// existing handle, therefore all require locking the same mutex.
//
// To minimize contention, each individual project is protected by its own mutex, and each handle
// maintains a direct pointer to this project. This allows the handle to access the project
// without locking the entire project registry.
#[derive(Clone, Debug)]
pub struct ProjectRegistry {
    inner: Arc<Mutex<ProjectRegistryInner>>,
}

impl ProjectRegistry {
    // Creates a new project registry.
    pub fn new() -> ProjectRegistry {
        ProjectRegistry {
            inner: Arc::new(Mutex::new(ProjectRegistryInner {
                rng: StdRng::from_entropy(),
                projects_by_id: HashMap::new(),
            })),
        }
    }

    // Creates a new project in this project registry, and returns both an identifier and a handle
    // for the new project. The identifier can be used as a key for the project, whereas the handle
    // can be used to access the project.
    pub fn create_project(&self) -> (ProjectId, ProjectHandle) {
        let mut manager_inner = self.inner.lock().unwrap();
        let id = manager_inner.rng.gen::<ProjectId>();
        let mut project = Project {
            ref_count: 1,
            inner: Box::pin(Mutex::new(ProjectInner {
                id,
                next_member_id: 0,
                members_by_id: HashMap::new(),
                next_file_id: 0,
                files_by_id: HashMap::new(),
                file_ids_by_path: HashMap::new(),
                committed_operations: Vec::new(),
            })),
        };
        // Increment the handle count, since we're about to create a new handle.
        project.ref_count += 1;
        // Obtain a pointer to the inner project type. This pointer will remain valid for the
        // lifetime of the handle we're about to create.
        let inner = NonNull::from(&*project.inner);
        manager_inner.projects_by_id.insert(id, project);
        (
            id,
            ProjectHandle {
                manager_inner: self.inner.clone(),
                id,
                inner,
            },
        )
    }

    // Returns a new handle for the project with the given identifier.
    pub fn get_project(&self, id: ProjectId) -> Option<ProjectHandle> {
        let mut manager_inner = self.inner.lock().unwrap();
        let project = manager_inner.projects_by_id.get_mut(&id)?;
        // Increment the handle count, since we're about to create a new handle.
        project.ref_count += 1;
        // Obtain a pointer to the inner project type. This pointer will remain valid for the
        // lifetime of the handle we're about to create.
        let inner = NonNull::from(&*project.inner);
        Some(ProjectHandle {
            manager_inner: self.inner.clone(),
            id,
            inner,
        })
    }
}

/// A clonable handle to a project.
#[derive(Debug)]
pub struct ProjectHandle {
    manager_inner: Arc<Mutex<ProjectRegistryInner>>,
    id: ProjectId,
    inner: NonNull<Mutex<ProjectInner>>,
}

impl ProjectHandle {
    // Joins this project and returns both a token representing the new membership of the project,
    // and a snapshot of the project state.
    //
    // For the duration of the membership, notifications for this project will be sent using the
    // sender `notification_sender`. To leave the project again, simply drop the membership token.
    // This also stops notifications from being sent.
    pub fn join(
        &self,
        notification_sender: Sender<Arc<Notification>>,
    ) -> (Membership, ProjectSnapshot) {
        let mut inner = self.inner().lock().unwrap();
        let member_id = inner.next_member_id;
        inner.next_member_id += 1;
        inner.members_by_id.insert(
            member_id,
            Member {
                notification_sender,
            },
        );
        for file in inner.files_by_id.values_mut() {
            file.revision_ids_by_member_id
                .insert(member_id, file.revision_id);
        }
        (
            Membership {
                project_handle: self.clone(),
                member_id,
            },
            ProjectSnapshot {
                files_by_id: {
                    let mut files_by_id = HashMap::new();
                    for (id, file) in &inner.files_by_id {
                        files_by_id.insert(
                            *id,
                            FileSnapshot {
                                outstanding_deltas: file
                                    .outstanding_deltas
                                    .iter()
                                    .cloned()
                                    .collect::<Vec<_>>(),
                            },
                        );
                    }
                    files_by_id
                },
                file_ids_by_path: inner.file_ids_by_path.clone(),
                committed_operations: inner.committed_operations.clone(),
            },
        )
    }

    fn inner(&self) -> &Mutex<ProjectInner> {
        // This is safe because `self.inner` refers to a pinned field, and the existence of this
        // handle guarantees that this field is alive.
        unsafe { self.inner.as_ref() }
    }
}

impl Clone for ProjectHandle {
    fn clone(&self) -> ProjectHandle {
        let manager_inner = &mut self.manager_inner.lock().unwrap();
        let project = manager_inner.projects_by_id.get_mut(&self.id).unwrap();
        // Increment the handle count, since we're about to create a new handle.
        project.ref_count += 1;
        ProjectHandle {
            manager_inner: self.manager_inner.clone(),
            id: self.id,
            inner: self.inner,
        }
    }
}

impl Drop for ProjectHandle {
    fn drop(&mut self) {
        let manager_inner = &mut self.manager_inner.lock().unwrap();
        let project = manager_inner.projects_by_id.get_mut(&self.id).unwrap();
        // Decrement the handle count, since we're about to drop the handle.
        project.ref_count -= 1;
        if project.ref_count == 0 {
            manager_inner.projects_by_id.remove(&self.id);
        }
    }
}

// A token representing membership of a project.
//
// Operations on a project that require membership of that project can only be accessed via a
// membership token.
#[derive(Debug)]
pub struct Membership {
    project_handle: ProjectHandle,
    member_id: MemberId,
}

impl Membership {
    pub fn create_file(&self, path: PathBuf) -> Result<(), Error> {
        let mut inner = self.project_handle.inner().lock().unwrap();
        if inner.file_ids_by_path.contains_key(&path) {
            return Err(Error::FileAlreadyExists);
        }
        let file_id = inner.next_file_id;
        inner.next_file_id += 1;
        let mut revision_ids_by_member_id = HashMap::new();
        for member_id in inner.members_by_id.keys() {
            revision_ids_by_member_id.insert(*member_id, 0);
        }
        inner.files_by_id.insert(
            file_id,
            File {
                revision_id: 0,
                revision_ids_by_member_id,
                outstanding_deltas: VecDeque::new(),
            },
        );
        inner.file_ids_by_path.insert(path.clone(), file_id);
        inner.commit_operation(Some(self.member_id), Operation::CreateFile(path));
        Ok(())
    }

    pub fn delete_file(&self, path: PathBuf) -> Result<(), Error> {
        let mut inner = self.project_handle.inner().lock().unwrap();
        if !inner.file_ids_by_path.contains_key(&path) {
            return Err(Error::FileNotFound);
        }
        inner.file_ids_by_path.remove(&path);
        inner.commit_operation(Some(self.member_id), Operation::DeleteFile(path));
        Ok(())
    }

    pub fn update_file(
        &self,
        file_id: FileId,
        revision_id: RevisionId,
        delta: Option<Delta>,
    ) -> Result<(), Error> {
        let inner = &mut *self.project_handle.inner().lock().unwrap();
        let file = inner
            .files_by_id
            .get_mut(&file_id)
            .ok_or(Error::FileNotFound)?;
        if revision_id < file.revision_ids_by_member_id[&self.member_id]
            || revision_id > file.revision_id
        {
            return Err(Error::RevisionNotFound);
        }
        *file
            .revision_ids_by_member_id
            .get_mut(&self.member_id)
            .unwrap() = revision_id;
        if let Some(mut delta) = delta {
            for outstanding_delta in file
                .outstanding_deltas
                .iter()
                .skip(file.outstanding_deltas.len() - (file.revision_id - revision_id))
            {
                let (_, new_delta) = outstanding_delta.clone().transform(delta);
                delta = new_delta;
            }
            file.revision_id += 1;
            file.outstanding_deltas.push_back(delta.clone());
            inner.notify_members(
                Some(self.member_id),
                Notification::FileUpdated(inner.id, file_id, delta),
            );
        }
        inner.handle_settled_deltas(file_id);
        Ok(())
    }
}

impl Drop for Membership {
    fn drop(&mut self) {
        let mut inner = self.project_handle.inner().lock().unwrap();
        inner.members_by_id.remove(&self.member_id);
        let file_ids = inner.files_by_id.keys().cloned().collect::<Vec<_>>();
        for file_id in file_ids {
            inner.handle_settled_deltas(file_id);
        }
    }
}

// The inner project manager type.
#[derive(Debug)]
struct ProjectRegistryInner {
    rng: StdRng,
    projects_by_id: HashMap<ProjectId, Project>,
}

#[derive(Debug)]
struct Project {
    // The number of handles to this project.
    ref_count: usize,
    // The inner project type. Since handles store a direct pointer to this field, it should be pinned.
    inner: Pin<Box<Mutex<ProjectInner>>>,
}

// The inner project type.
#[derive(Debug)]
struct ProjectInner {
    id: ProjectId,
    next_member_id: MemberId,
    members_by_id: HashMap<MemberId, Member>,
    next_file_id: FileId,
    files_by_id: HashMap<FileId, File>,
    file_ids_by_path: HashMap<PathBuf, FileId>,
    committed_operations: Vec<Operation>,
}

impl ProjectInner {
    fn handle_settled_deltas(&mut self, file_id: FileId) {
        let file = self.files_by_id.get_mut(&file_id).unwrap();
        let revision_id = file.revision_ids_by_member_id.values().min().unwrap();
        let settled_deltas = file
            .outstanding_deltas
            .drain(..file.outstanding_deltas.len() - (file.revision_id - revision_id))
            .collect::<Vec<_>>();
        for settled_delta in settled_deltas {
            self.commit_operation(None, Operation::UpdateFile(file_id, settled_delta))
        }
    }

    fn commit_operation(&mut self, self_id: Option<MemberId>, operation: Operation) {
        self.committed_operations.push(operation.clone());
        self.notify_members(
            self_id,
            Notification::OperationCommitted(self.id, operation),
        );
    }

    fn notify_members(&mut self, self_id: Option<MemberId>, notification: Notification) {
        let notification = Arc::new(notification);
        for (member_id, member) in self.members_by_id.iter() {
            match self_id {
                Some(self_id) if self_id == *member_id => {
                    continue;
                }
                _ => {}
            }
            member
                .notification_sender
                .send(notification.clone())
                .unwrap();
        }
    }
}

// An identifier for a member of a project.
type MemberId = usize;

// A member of a project.
#[derive(Debug)]
struct Member {
    notification_sender: Sender<Arc<Notification>>,
}

// A file in a project.
#[derive(Debug)]
struct File {
    // Our current revision id.
    revision_id: RevisionId,
    // The last revision id seen by each member.
    revision_ids_by_member_id: HashMap<MemberId, RevisionId>,
    // A queue of deltas that has yet not been seen by every member.
    outstanding_deltas: VecDeque<Delta>,
}
