use {
    crate::project::{Membership, ProjectRegistry},
    collab_protocol::{
        Error, Notification, ProjectId, ProjectSnapshot, Request, RequestBody, Response,
        ResponseBody,
    },
    core::{
        operation::{FileId, RevisionId},
        path::PathBuf,
        Delta,
    },
    std::{
        collections::HashMap,
        sync::{mpsc::Sender, Arc},
    },
};

/// A collab server.
#[derive(Debug)]
pub struct Server {
    project_manager: ProjectRegistry,
}

impl Server {
    /// Creates a new collab server.
    pub fn new() -> Server {
        Server {
            project_manager: ProjectRegistry::new(),
        }
    }

    /// Accepts a new connection to this collab server.
    pub fn accept(&self, notification_sender: Sender<Arc<Notification>>) -> Handler {
        Handler {
            notification_sender,
            project_manager: self.project_manager.clone(),
            memberships_by_project_id: HashMap::new(),
        }
    }
}

/// A handler for a connection.
#[derive(Debug)]
pub struct Handler {
    notification_sender: Sender<Arc<Notification>>,
    project_manager: ProjectRegistry,
    memberships_by_project_id: HashMap<ProjectId, Membership>,
}

impl Handler {
    /// Handles a request from the client to this server and returns a response.
    pub fn handle_request(&mut self, request: Request) -> Response {
        Response {
            id: request.id,
            body: match request.body {
                RequestBody::CreateProject() => ResponseBody::CreateProject(self.create_project()),
                RequestBody::JoinProject(project_id) => {
                    ResponseBody::JoinProject(self.join_project(project_id))
                }
                RequestBody::LeaveProject(project_id) => {
                    ResponseBody::LeaveProject(self.leave_project(project_id))
                }
                RequestBody::CreateFile(project_id, path) => {
                    ResponseBody::CreateFile(self.create_file(project_id, path))
                }
                RequestBody::DeleteFile(project_id, path) => {
                    ResponseBody::DeleteFile(self.delete_file(project_id, path))
                }
                RequestBody::UpdateFile(project_id, file_id, revision_id, delta) => {
                    ResponseBody::UpdateFile(self.update_file(
                        project_id,
                        file_id,
                        revision_id,
                        delta,
                    ))
                }
            },
        }
    }

    fn create_project(&mut self) -> ProjectId {
        let (project_id, project_handle) = self.project_manager.create_project();
        let (membership, _) = project_handle.join(self.notification_sender.clone());
        self.memberships_by_project_id
            .insert(project_id, membership);
        project_id
    }

    fn join_project(&mut self, project_id: ProjectId) -> Result<ProjectSnapshot, Error> {
        if self.memberships_by_project_id.contains_key(&project_id) {
            return Err(Error::AlreadyAMember);
        }
        let project_handle = self
            .project_manager
            .get_project(project_id)
            .ok_or(Error::ProjectNotFound)?;
        let (membership, snapshot) = project_handle.join(self.notification_sender.clone());
        self.memberships_by_project_id
            .insert(project_id, membership);
        Ok(snapshot)
    }

    fn leave_project(&mut self, project_id: ProjectId) -> Result<(), Error> {
        if !self.memberships_by_project_id.contains_key(&project_id) {
            return Err(Error::NotAMember);
        }
        self.memberships_by_project_id.remove(&project_id);
        Ok(())
    }

    fn create_file(&mut self, project_id: ProjectId, path: PathBuf) -> Result<(), Error> {
        let membership = self
            .memberships_by_project_id
            .get(&project_id)
            .ok_or(Error::NotAMember)?;
        membership.create_file(path)
    }

    fn delete_file(&mut self, project_id: ProjectId, path: PathBuf) -> Result<(), Error> {
        let membership = self
            .memberships_by_project_id
            .get(&project_id)
            .ok_or(Error::NotAMember)?;
        membership.delete_file(path)
    }

    fn update_file(
        &mut self,
        project_id: ProjectId,
        file_id: FileId,
        revision_id: RevisionId,
        delta: Option<Delta>,
    ) -> Result<(), Error> {
        let membership = self
            .memberships_by_project_id
            .get(&project_id)
            .ok_or(Error::NotAMember)?;
        membership.update_file(file_id, revision_id, delta)
    }
}
