use {
    crate::Size,
    std::ops::{Add, AddAssign, Sub},
};

/// A position in a text.
#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Position {
    pub line_index: usize,
    pub column_index: usize,
}

impl Position {
    /// Creates a position corresponding to the start of a text.
    pub fn start() -> Position {
        Position::default()
    }

    /// Converts this position to a size. This is equivalent to `self - Position::start()`.
    pub fn to_size(self) -> Size {
        Size {
            line_count: self.line_index,
            column_count: self.column_index,
        }
    }
}

impl Add<Size> for Position {
    type Output = Position;

    fn add(self, other: Size) -> Position {
        if other.line_count == 0 {
            Position {
                line_index: self.line_index,
                column_index: self.column_index + other.column_count,
            }
        } else {
            Position {
                line_index: self.line_index + other.line_count,
                column_index: other.column_count,
            }
        }
    }
}

impl AddAssign<Size> for Position {
    fn add_assign(&mut self, other: Size) {
        *self = *self + other;
    }
}

impl Sub for Position {
    type Output = Size;

    fn sub(self, other: Position) -> Size {
        if self.line_index == other.line_index {
            Size {
                line_count: 0,
                column_count: self.column_index - other.column_index,
            }
        } else {
            Size {
                line_count: self.line_index - other.line_index,
                column_count: self.column_index,
            }
        }
    }
}
