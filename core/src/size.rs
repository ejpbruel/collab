use std::ops::{Add, AddAssign, Sub, SubAssign};

/// A size type for use in a text.
///
/// A size is represented as the positive distance between the start and end of a text. That is, as
/// the number of lines one would have to walk downwards, followed by the number of columns one
/// would have to walk rightwards, in order to move from the start to the end of the text. This
/// representation was chosen to make a size easy to work with in the context of deltas, but makes
/// its behavior somewhat peculiar.
///
/// The number of characters represented by a size is context dependent: for different texts, and
/// different positions in that text, the same size represents a different number of characters in
/// that text. Moreover, a size can be added to a position to move that position forward, but not
/// subtracted from a position to move that position backwards.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Size {
    pub line_count: usize,
    pub column_count: usize,
}

impl Size {
    /// Returns `true` is this size is 0.
    pub fn is_zero(self) -> bool {
        self.line_count == 0 && self.column_count == 0
    }
}

impl Add for Size {
    type Output = Size;

    fn add(self, other: Size) -> Size {
        if other.line_count == 0 {
            Size {
                line_count: self.line_count,
                column_count: self.column_count + other.column_count,
            }
        } else {
            Size {
                line_count: self.line_count + other.line_count,
                column_count: other.column_count,
            }
        }
    }
}

impl AddAssign for Size {
    fn add_assign(&mut self, other: Size) {
        *self = *self + other;
    }
}

impl Sub for Size {
    type Output = Size;

    fn sub(self, other: Size) -> Size {
        if other.line_count == self.line_count {
            Size {
                line_count: 0,
                column_count: self.column_count - other.column_count,
            }
        } else {
            Size {
                line_count: self.line_count - other.line_count,
                column_count: self.column_count,
            }
        }
    }
}

impl SubAssign for Size {
    fn sub_assign(&mut self, other: Size) {
        *self = *self - other;
    }
}
