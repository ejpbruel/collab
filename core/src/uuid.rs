use std::fmt;

#[cfg(feature = "rand")]
use rand::{
    distributions::{Distribution, Standard},
    Rng,
};

/// A universally unique identifier.
#[derive(Clone, Copy, Debug, Default, Eq, Hash, PartialEq)]
pub struct Uuid([u8; 16]);

impl Uuid {
    /// Creates a UUID from its memory representation as a byte array.
    pub fn from_bytes(bytes: [u8; 16]) -> Uuid {
        Uuid(bytes)
    }

    /// Returns the memory representation of this UUID as a byte array.
    pub fn to_bytes(&self) -> [u8; 16] {
        self.0
    }
}

impl fmt::Display for Uuid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for byte in &self.0 {
            write!(f, "{:x}", *byte)?;
        }
        Ok(())
    }
}

#[cfg(feature = "rand")]
impl Distribution<Uuid> for Standard {
    fn sample<R: Rng>(&self, rng: &mut R) -> Uuid
    where
        R: Rng + ?Sized,
    {
        Uuid::from_bytes(rng.gen())
    }
}
