//! Path manipulation
//!
//! This module provides two types, PathBuf and Path (akin to String and str) for working with paths
//! abstractly. These types are roughly analogous to those with the same name from the path module
//! in the standard library, but instead of platform-native paths, they represent virtual paths to
//! files in a project.
//!
//! To make paths easy to work with, we always use the slash ('/') character as separator. A
//! component of a path is valid if and only if:
//! 1. It is not empty.
//! 2. It is not a single dot (".").
//! 3. It is not a double dot ("..").
//! 3. It does not contain a nul ('\0') character.
//! 4. It does not contain an unescaped backslash ('\') character.

use std::{borrow::Borrow, convert::AsRef, error::Error, fmt, ops::Deref};

/// An owned, mutable path (akin to String).
#[derive(Clone, Debug, Default, Eq, Hash, PartialEq)]
pub struct PathBuf {
    string: String,
}

impl PathBuf {
    /// Creates an empty path.
    pub fn new() -> PathBuf {
        PathBuf::default()
    }

    /// Creates a path from a string.
    ///
    /// # Errors
    ///
    /// Returns an error if the string is not a valid path.
    pub fn from_string(string: String) -> Result<PathBuf, ValidationError> {
        validate_path(&string)?;
        // This is safe because we just validated the path here above.
        Ok(unsafe { PathBuf::from_string_unchecked(string) })
    }

    /// Creates a path from a string without doing any validation.
    ///
    /// # Safety
    ///
    /// The string must be a valid path.
    pub unsafe fn from_string_unchecked(string: String) -> PathBuf {
        PathBuf { string }
    }
}

impl AsRef<Path> for PathBuf {
    fn as_ref(&self) -> &Path {
        self
    }
}

impl AsRef<str> for PathBuf {
    fn as_ref(&self) -> &str {
        &self.string
    }
}

impl Borrow<Path> for PathBuf {
    fn borrow(&self) -> &Path {
        self
    }
}

impl Deref for PathBuf {
    type Target = Path;

    fn deref(&self) -> &Path {
        // This is safe because this is a valid path.
        unsafe { Path::from_str_unchecked(&self.string) }
    }
}

/// A slice of a path (akin to str).
#[derive(Debug, Eq, Hash, PartialEq)]
#[repr(transparent)]
pub struct Path {
    string: str,
}

impl Path {
    /// Creates a path from a string.
    ///
    /// # Errors
    ///
    /// Returns an error if the string is not a valid path.
    pub fn from_str(string: &str) -> Result<&Path, ValidationError> {
        validate_path(&string)?;
        Ok(unsafe { Path::from_str_unchecked(string) })
    }

    /// Creates a path from a string without doing any validation.
    ///
    /// # Safety
    ///
    /// The string must be a valid path.
    pub unsafe fn from_str_unchecked(string: &str) -> &Path {
        &*(string as *const str as *const Path)
    }
}

impl AsRef<str> for Path {
    fn as_ref(&self) -> &str {
        &self.string
    }
}

impl ToOwned for Path {
    type Owned = PathBuf;

    fn to_owned(&self) -> PathBuf {
        PathBuf {
            string: self.string.to_owned(),
        }
    }
}

/// An error when validating a path.
#[derive(Debug)]
pub struct ValidationError(());

impl fmt::Display for ValidationError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "string is not a valid path")
    }
}

impl Error for ValidationError {}

fn validate_path(string: &str) -> Result<(), ValidationError> {
    if string.is_empty() {
        return Ok(());
    }
    for component in string.split('/') {
        match component {
            "" | "." | ".." => return Err(ValidationError(())),
            _ => {}
        }
        let mut chars = component.chars();
        loop {
            match chars.next() {
                Some('\0') => return Err(ValidationError(())),
                Some('\\') => match chars.next() {
                    Some('\\') => {}
                    _ => return Err(ValidationError(())),
                },
                Some(_) => {}
                None => break,
            }
        }
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_validate_path() {
        assert!(validate_path("").is_ok());
        assert!(validate_path("\\\\").is_ok());
        assert!(validate_path("abc/def/ghi").is_ok());
        assert!(validate_path("abc/d.f/ghi").is_ok());
        assert!(validate_path("abc/d..f/ghi").is_ok());
        assert!(validate_path("/").is_err());
        assert!(validate_path(".").is_err());
        assert!(validate_path("..").is_err());
        assert!(validate_path("\0").is_err());
        assert!(validate_path("\\").is_err());
        assert!(validate_path("abc//ghi").is_err());
        assert!(validate_path("abc/./ghi").is_err());
        assert!(validate_path("abc/../ghi").is_err());
    }
}
