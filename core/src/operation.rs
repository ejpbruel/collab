use crate::{delta::Delta, path::PathBuf};

/// An operation that can be applied to a project.
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Operation {
    CreateFile(PathBuf),
    DeleteFile(PathBuf),
    UpdateFile(FileId, Delta),
}

/// An identifier for a file in a project.
pub type FileId = usize;

/// An identifier for a revision in a file.
pub type RevisionId = usize;
