use {
    crate::{Position, Size, Text},
    std::{cmp::Ordering, mem},
};

/// A change to a text.
///
/// A delta is represented as a vector of operations. Each operation operates on an imaginary
/// cursor: it either moves the cursor forward, or inserts/deletes a (sub)text at the current cursor
/// position. This representation was chosen to make operations on a delta, such as composition and
/// transformation, easy to implement.
///
/// A delta has the following invariants:
/// 1. It contains no operations that have no effect.
/// 2. Every delete operation is succeeded by an insert operation.
#[derive(Clone, Debug, Default, Eq, Hash)]
pub struct Delta {
    operations: Vec<Operation>,
}

impl Delta {
    /// Creates a delta that has no effect.
    pub fn identity() -> Delta {
        Delta::default()
    }

    /// Creates a delta that inserts a text `text` at a position `pos` when applied to a text.
    pub fn insert(pos: Position, text: Text) -> Delta {
        let mut builder = Builder::new();
        builder.retain(pos.to_size());
        builder.insert(text);
        builder.build()
    }

    /// Creates a delta that deletes a subetext of length `len` at a position `pos` when applied to
    /// a text.
    pub fn delete(pos: Position, len: Size) -> Delta {
        let mut builder = Builder::new();
        builder.retain(pos.to_size());
        builder.delete(len);
        builder.build()
    }

    /// Returns a slice to the underlying vector of operations.
    pub fn as_operations(&self) -> &[Operation] {
        &self.operations
    }

    /// Consumes this delta and returns the underlying vector of operations.
    pub fn into_operations(self) -> Vec<Operation> {
        self.operations
    }

    /// Consumes this delta A and another delta B that happened sequentially, and returns a delta C
    /// such that applying C to a text has the same effect as first applying A and then B.
    pub fn compose(self, other: Delta) -> Delta {
        let mut builder = Builder::new();
        let mut operation_iter_0 = self.operations.into_iter();
        let mut operation_iter_1 = other.operations.into_iter();
        let mut next_operation_0 = operation_iter_0.next();
        let mut next_operation_1 = operation_iter_1.next();
        loop {
            match (next_operation_0, next_operation_1) {
                (Some(Operation::Delete(len)), operation) => {
                    builder.delete(len);
                    next_operation_0 = operation_iter_0.next();
                    next_operation_1 = operation;
                }
                (operation, Some(Operation::Insert(text))) => {
                    builder.insert(text);
                    next_operation_0 = operation;
                    next_operation_1 = operation_iter_1.next();
                }
                (Some(Operation::Retain(len_0)), Some(Operation::Retain(len_1))) => {
                    match len_0.cmp(&len_1) {
                        Ordering::Less => {
                            builder.retain(len_0);
                            next_operation_0 = operation_iter_0.next();
                            next_operation_1 = Some(Operation::Retain(len_1 - len_0));
                        }
                        Ordering::Equal => {
                            builder.retain(len_0);
                            next_operation_0 = operation_iter_0.next();
                            next_operation_1 = operation_iter_1.next();
                        }
                        Ordering::Greater => {
                            builder.retain(len_1);
                            next_operation_0 = Some(Operation::Retain(len_0 - len_1));
                            next_operation_1 = operation_iter_1.next();
                        }
                    }
                }
                (Some(Operation::Retain(len_0)), Some(Operation::Delete(len_1))) => {
                    match len_0.cmp(&len_1) {
                        Ordering::Less => {
                            builder.delete(len_0);
                            next_operation_0 = operation_iter_0.next();
                            next_operation_1 = Some(Operation::Delete(len_1 - len_0));
                        }
                        Ordering::Equal => {
                            builder.delete(len_0);
                            next_operation_0 = operation_iter_0.next();
                            next_operation_1 = operation_iter_1.next();
                        }
                        Ordering::Greater => {
                            builder.delete(len_1);
                            next_operation_0 = Some(Operation::Retain(len_0 - len_1));
                            next_operation_1 = operation_iter_1.next();
                        }
                    }
                }
                (Some(Operation::Retain(len)), None) => {
                    builder.retain(len);
                    next_operation_0 = operation_iter_0.next();
                    next_operation_1 = None;
                }
                (Some(Operation::Insert(mut text)), Some(Operation::Retain(len))) => {
                    match text.len().cmp(&len) {
                        Ordering::Less => {
                            let text_len = text.len();
                            builder.insert(text);
                            next_operation_0 = operation_iter_0.next();
                            next_operation_1 = Some(Operation::Retain(len - text_len));
                        }
                        Ordering::Equal => {
                            builder.insert(text);
                            next_operation_0 = operation_iter_0.next();
                            next_operation_1 = operation_iter_1.next();
                        }
                        Ordering::Greater => {
                            builder.insert(text.take(len));
                            next_operation_0 = Some(Operation::Insert(text));
                            next_operation_1 = operation_iter_1.next();
                        }
                    }
                }
                (Some(Operation::Insert(mut text)), Some(Operation::Delete(len))) => {
                    match text.len().cmp(&len) {
                        Ordering::Less => {
                            next_operation_0 = operation_iter_0.next();
                            next_operation_1 = Some(Operation::Delete(len - text.len()));
                        }
                        Ordering::Equal => {
                            next_operation_0 = operation_iter_0.next();
                            next_operation_1 = operation_iter_1.next();
                        }
                        Ordering::Greater => {
                            text.skip(len);
                            next_operation_0 = Some(Operation::Insert(text));
                            next_operation_1 = operation_iter_1.next();
                        }
                    }
                }
                (Some(Operation::Insert(text)), None) => {
                    builder.insert(text);
                    next_operation_0 = operation_iter_0.next();
                    next_operation_1 = None;
                }
                (None, Some(Operation::Retain(len))) => {
                    builder.retain(len);
                    next_operation_0 = None;
                    next_operation_1 = operation_iter_1.next();
                }
                (None, Some(Operation::Delete(len))) => {
                    builder.delete(len);
                    next_operation_0 = None;
                    next_operation_1 = operation_iter_1.next();
                }
                (None, None) => break,
            }
        }
        builder.build()
    }

    /// Consumes this delta A and another delta B that happened concurrently, and returns a pair of
    /// deltas (A', B') such that first applying A and then B' to a text has the same effect as
    /// first applying B and then A'.
    pub fn transform(self, other: Delta) -> (Delta, Delta) {
        let mut builder_0 = Builder::new();
        let mut builder_1 = Builder::new();
        let mut operation_iter_0 = self.operations.into_iter();
        let mut operation_iter_1 = other.operations.into_iter();
        let mut next_operation_0 = operation_iter_0.next();
        let mut next_operation_1 = operation_iter_1.next();
        loop {
            match (next_operation_0, next_operation_1) {
                (Some(Operation::Insert(text)), operation) => {
                    let text_len = text.len();
                    builder_0.insert(text);
                    builder_1.retain(text_len);
                    next_operation_0 = operation_iter_0.next();
                    next_operation_1 = operation;
                }
                (operation, Some(Operation::Insert(text))) => {
                    builder_0.retain(text.len());
                    builder_1.insert(text);
                    next_operation_0 = operation;
                    next_operation_1 = operation_iter_1.next();
                }
                (Some(Operation::Retain(len_0)), Some(Operation::Retain(len_1))) => {
                    match len_0.cmp(&len_1) {
                        Ordering::Less => {
                            builder_0.retain(len_0);
                            builder_1.retain(len_0);
                            next_operation_0 = operation_iter_0.next();
                            next_operation_1 = Some(Operation::Retain(len_1 - len_0));
                        }
                        Ordering::Equal => {
                            builder_0.retain(len_0);
                            builder_1.retain(len_0);
                            next_operation_0 = operation_iter_0.next();
                            next_operation_1 = operation_iter_1.next();
                        }
                        Ordering::Greater => {
                            builder_0.retain(len_1);
                            builder_1.retain(len_1);
                            next_operation_0 = Some(Operation::Retain(len_0 - len_1));
                            next_operation_1 = operation_iter_1.next();
                        }
                    };
                }
                (Some(Operation::Retain(len_0)), Some(Operation::Delete(len_1))) => {
                    match len_0.cmp(&len_1) {
                        Ordering::Less => {
                            builder_1.delete(len_0);
                            next_operation_0 = operation_iter_0.next();
                            next_operation_1 = Some(Operation::Delete(len_1 - len_0));
                        }
                        Ordering::Equal => {
                            builder_1.delete(len_0);
                            next_operation_0 = operation_iter_0.next();
                            next_operation_1 = operation_iter_1.next();
                        }
                        Ordering::Greater => {
                            builder_1.delete(len_1);
                            next_operation_0 = Some(Operation::Retain(len_0 - len_1));
                            next_operation_1 = operation_iter_1.next();
                        }
                    };
                }
                (Some(Operation::Retain(len)), None) => {
                    builder_0.retain(len);
                    builder_1.retain(len);
                    next_operation_0 = operation_iter_0.next();
                    next_operation_1 = None;
                }
                (Some(Operation::Delete(len_0)), Some(Operation::Retain(len_1))) => {
                    match len_0.cmp(&len_1) {
                        Ordering::Less => {
                            builder_0.delete(len_0);
                            next_operation_0 = operation_iter_0.next();
                            next_operation_1 = Some(Operation::Retain(len_1 - len_0));
                        }
                        Ordering::Equal => {
                            builder_0.delete(len_0);
                            next_operation_0 = operation_iter_0.next();
                            next_operation_1 = operation_iter_1.next();
                        }
                        Ordering::Greater => {
                            builder_0.delete(len_1);
                            next_operation_0 = Some(Operation::Delete(len_0 - len_1));
                            next_operation_1 = operation_iter_1.next();
                        }
                    };
                }
                (Some(Operation::Delete(len_0)), Some(Operation::Delete(len_1))) => {
                    match len_0.cmp(&len_1) {
                        Ordering::Less => {
                            next_operation_0 = operation_iter_0.next();
                            next_operation_1 = Some(Operation::Delete(len_1 - len_0));
                        }
                        Ordering::Equal => {
                            next_operation_0 = operation_iter_0.next();
                            next_operation_1 = operation_iter_1.next();
                        }
                        Ordering::Greater => {
                            next_operation_0 = Some(Operation::Delete(len_0 - len_1));
                            next_operation_1 = operation_iter_1.next();
                        }
                    }
                }
                (Some(Operation::Delete(len)), None) => {
                    builder_0.delete(len);
                    next_operation_0 = operation_iter_0.next();
                    next_operation_1 = None;
                }
                (None, Some(Operation::Retain(len))) => {
                    builder_0.retain(len);
                    builder_1.retain(len);
                    next_operation_0 = None;
                    next_operation_1 = operation_iter_1.next();
                }
                (None, Some(Operation::Delete(len))) => {
                    builder_1.delete(len);
                    next_operation_0 = None;
                    next_operation_1 = operation_iter_1.next();
                }
                (None, None) => break,
            }
        }
        (builder_0.build(), builder_1.build())
    }
}

impl PartialEq for Delta {
    fn eq(&self, other: &Delta) -> bool {
        let mut operation_iter_0 = self.operations.iter();
        let mut operation_iter_1 = other.operations.iter();
        let mut next_operation_0 = operation_iter_0.next();
        let mut next_operation_1 = operation_iter_1.next();
        loop {
            match (next_operation_0, next_operation_1) {
                (Some(Operation::Retain(_)), None) => {
                    next_operation_0 = operation_iter_0.next();
                    next_operation_1 = None;
                }
                (None, Some(Operation::Retain(_))) => {
                    next_operation_0 = None;
                    next_operation_1 = operation_iter_1.next();
                }
                (Some(operation_0), Some(operation_1)) => {
                    if operation_0 != operation_1 {
                        return false;
                    }
                    next_operation_0 = operation_iter_0.next();
                    next_operation_1 = operation_iter_1.next();
                }
                (Some(_), None) | (None, Some(_)) => {
                    return false;
                }
                (None, None) => break,
            }
        }
        true
    }
}

/// A builder for deltas.
#[derive(Debug, Default)]
pub struct Builder {
    operations: Vec<Operation>,
}

impl Builder {
    /// Creates a new builder.
    pub fn new() -> Builder {
        Builder::default()
    }

    /// Appends a retain operation.
    pub fn retain(&mut self, len: Size) {
        // To maintain the invariant that a delta contains no operations that have no effect, we do
        // not append the operation if it has no effect.
        if len.is_zero() {
            return;
        }
        match self.operations.last_mut() {
            Some(Operation::Retain(last_len)) => *last_len += len,
            _ => self.operations.push(Operation::Retain(len)),
        }
    }

    /// Appends an insert operation.
    pub fn insert(&mut self, text: Text) {
        // To maintain the invariant that a delta contains no operations that have no effect, we do
        // not append the new operation if it has no effect.
        if text.is_empty() {
            return;
        }
        let operation = match self.operations.as_mut_slice() {
            [.., Operation::Insert(last_text)] => {
                *last_text += text;
                None
            }
            // To maintain the invariant that every delete operation is succeeded by an insert
            // operation, we insert the operation before the last operation if the latter is a
            // a delete operation.
            //
            // Note that this does not change the behavior of the resulting delta, since insert and
            // delete operations are commutative. That is, insert(text), delete(len) has the same
            // effect as delete(len), insert(text).
            [.., Operation::Insert(second_to_last_text), Operation::Delete(_)] => {
                *second_to_last_text += text;
                None
            }
            [.., last_operation @ Operation::Delete(_)] => {
                Some(mem::replace(last_operation, Operation::Insert(text)))
            }
            _ => Some(Operation::Insert(text)),
        };
        if let Some(operation) = operation {
            self.operations.push(operation);
        }
    }

    /// Appends a delete operation.
    pub fn delete(&mut self, len: Size) {
        // To maintain the invariant that a delta contains no operations that have no effect, we do
        // not append the operation if it has no effect.
        if len.is_zero() {
            return;
        }
        match self.operations.last_mut() {
            Some(Operation::Delete(last_len)) => *last_len += len,
            _ => self.operations.push(Operation::Delete(len)),
        }
    }

    /// Consumes the builder and creates a new delta.
    pub fn build(self) -> Delta {
        Delta {
            operations: self.operations,
        }
    }
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum Operation {
    /// Moves the cursor forward by the given distance.
    Retain(Size),
    /// Inserts the given text at the current cursor position.
    Insert(Text),
    /// Deletes a subtext of the given length at the current cursor position.
    Delete(Size),
}

#[cfg(test)]
mod tests {
    use {
        super::*,
        rand::{distributions::Alphanumeric, rngs::StdRng, Rng, SeedableRng},
        std::iter,
    };

    // Generates an arbitrary text.
    fn gen_text<R>(rng: &mut R) -> Text
    where
        R: Rng + ?Sized,
    {
        let line_count = rng.gen_range(0, 11);
        Text::from_lines(
            iter::repeat(())
                .map(|()| {
                    let column_count = rng.gen_range(0, 11);
                    iter::repeat(())
                        .map(|()| rng.sample(Alphanumeric))
                        .take(column_count)
                        .collect::<Vec<char>>()
                })
                .take(line_count)
                .collect::<Vec<Vec<char>>>(),
        )
        .unwrap()
    }

    /// Generates an arbitrary delta that is valid for the text `text`.
    fn gen_delta_for_text<R>(rng: &mut R, text: &Text) -> Delta
    where
        R: Rng + ?Sized,
    {
        let mut builder = Builder::new();
        let mut pos = Position::start();
        for _ in 0..rng.gen_range(0, 11) {
            match rng.gen_range(0, 3) {
                0 => {
                    let len = gen_size_for_text_at_position(rng, text, pos);
                    builder.retain(len);
                    pos += len;
                }
                1 => {
                    builder.insert(gen_text(rng));
                }
                _ => {
                    let len = gen_size_for_text_at_position(rng, text, pos);
                    builder.delete(len);
                    pos += len;
                }
            }
        }
        builder.build()
    }

    // Generates an arbitrary size that is valid for the text `text` at the position `pos`.
    fn gen_size_for_text_at_position<R>(rng: &mut R, text: &Text, pos: Position) -> Size
    where
        R: Rng + ?Sized,
    {
        let lines = text.as_lines();
        let line_count = rng.gen_range(0, lines[pos.line_index..].len());
        Size {
            line_count,
            column_count: rng.gen_range(
                0,
                if line_count == 0 {
                    lines[pos.line_index][pos.column_index..].len()
                } else {
                    lines[pos.line_index + line_count].len()
                } + 1,
            ),
        }
    }

    #[test]
    fn test_compose() {
        let mut rng = StdRng::seed_from_u64(0);
        for _ in 0..1024 {
            let text_0 = gen_text(&mut rng);
            let delta_0 = gen_delta_for_text(&mut rng, &text_0);
            let mut text_1 = text_0.clone();
            text_1.apply_delta(delta_0.clone());
            let delta_1 = gen_delta_for_text(&mut rng, &text_1);
            let mut text_2 = text_1.clone();
            text_2.apply_delta(delta_1.clone());
            let delta_3 = delta_0.compose(delta_1);
            let mut text_3 = text_0.clone();
            text_3.apply_delta(delta_3);
            assert_eq!(text_2, text_3);
        }
    }

    #[test]
    fn test_transform() {
        let mut rng = StdRng::seed_from_u64(0);
        for _ in 0..1024 {
            let text_0 = gen_text(&mut rng);
            let delta_0 = gen_delta_for_text(&mut rng, &text_0);
            let delta_1 = gen_delta_for_text(&mut rng, &text_0);
            let (delta_2, delta_3) = delta_0.clone().transform(delta_1.clone());
            let mut text_1 = text_0.clone();
            text_1.apply_delta(delta_0);
            text_1.apply_delta(delta_3);
            let mut text_2 = text_0.clone();
            text_2.apply_delta(delta_1);
            text_2.apply_delta(delta_2);
            assert_eq!(text_1, text_2);
        }
    }
}
