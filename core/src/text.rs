use {
    crate::{delta::Operation, Delta, Position, Size},
    std::{error::Error, fmt, iter, mem, ops::AddAssign, str::FromStr},
};

/// A text.
///
/// A text is represented as a vector of lines, where each line is a vector of chars. This
/// representation was chosen to make operations on a text easy to implement, rather than to
/// minimize the amount of storage required. For most real life projects, the amount of storage
/// required for a text is not expected to pose any problems.
///
/// A text has the following invariants:
/// 1. It contains at least one line.
/// 2. Every line contains neither nul ('\0') nor newline ('\n') characters.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Text {
    lines: Vec<Vec<char>>,
}

impl Text {
    /// Creates an empty text.
    pub fn new() -> Text {
        Text::default()
    }

    /// Creates a text from a vector of lines, ensuring that there is at least one line..
    ///
    /// # Errors
    ///
    /// Returns if a line contains either nul (`\0`) or newline ('\n') characters.
    pub fn from_lines(mut lines: Vec<Vec<char>>) -> Result<Text, FromLinesError> {
        if lines.is_empty() {
            lines.push(Vec::new());
        }
        if lines
            .iter()
            .any(|line| line.iter().any(|ch| *ch == '\0' || *ch == '\n'))
        {
            return Err(FromLinesError);
        }
        // This is safe because we just checked the invariants here above.
        Ok(unsafe { Text::from_lines_unchecked(lines) })
    }

    /// Creates a text from a vector of lines without checking any invariants.
    ///
    /// # Safety
    ///
    /// The vector must be non-empty, and every line must contain neither nul ('\0') nor newline
    /// ('\n') characters.
    pub unsafe fn from_lines_unchecked(lines: Vec<Vec<char>>) -> Text {
        Text { lines }
    }

    /// Returns `true` if this text is empty.
    pub fn is_empty(&self) -> bool {
        self.len().is_zero()
    }

    /// Returns the length of this text.
    pub fn len(&self) -> Size {
        Size {
            line_count: self.lines.len() - 1,
            column_count: self.lines.last().unwrap().len(),
        }
    }

    /// Returns a slice to the underlying vector of lines.
    pub fn as_lines(&self) -> &[Vec<char>] {
        &self.lines
    }

    /// Deletes and returns a subtext of length `len` at the start of this text.
    pub fn take(&mut self, len: Size) -> Text {
        let mut lines = self.lines.drain(..len.line_count).collect::<Vec<_>>();
        lines.push(
            self.lines
                .first_mut()
                .unwrap()
                .drain(..len.column_count)
                .collect::<Vec<_>>(),
        );
        Text { lines }
    }

    /// Deletes a subtext of length `len` at the start of this text.
    pub fn skip(&mut self, len: Size) {
        self.lines.drain(..len.line_count);
        self.lines.first_mut().unwrap().drain(..len.column_count);
    }

    /// Inserts a text `text` at a position `pos` in this text.
    pub fn insert(&mut self, pos: Position, mut text: Text) {
        if text.len().line_count == 0 {
            self.lines[pos.line_index].splice(
                pos.column_index..pos.column_index,
                text.lines.first().unwrap().iter().cloned(),
            );
        } else {
            text.lines.first_mut().unwrap().splice(
                ..0,
                self.lines[pos.line_index][..pos.column_index]
                    .iter()
                    .cloned(),
            );
            text.lines.last_mut().unwrap().extend(
                self.lines[pos.line_index][pos.column_index..]
                    .iter()
                    .cloned(),
            );
            self.lines
                .splice(pos.line_index..pos.line_index + 1, text.lines.into_iter());
        }
    }

    // Deletes a subtext of length `len` at a position `pos` in this text.
    pub fn delete(&mut self, pos: Position, len: Size) {
        if len.line_count == 0 {
            self.lines[pos.line_index].drain(pos.column_index..pos.column_index + len.column_count);
        } else {
            let mut line = mem::replace(&mut self.lines[pos.line_index], Vec::new());
            line.splice(
                pos.column_index..,
                self.lines[pos.line_index + len.line_count][len.column_count..]
                    .iter()
                    .cloned(),
            );
            self.lines.splice(
                pos.line_index..pos.line_index + len.line_count + 1,
                iter::once(line),
            );
        }
    }

    /// Applies a delta `delta` to this text.
    pub fn apply_delta(&mut self, delta: Delta) {
        let mut pos = Position::start();
        for operation in delta.into_operations() {
            match operation {
                Operation::Retain(len) => pos += len,
                Operation::Insert(text) => {
                    let len = text.len();
                    self.insert(pos, text);
                    pos += len;
                }
                Operation::Delete(len) => self.delete(pos, len),
            }
        }
    }

    /// Consumes this text and returns the underlying vector of lines.
    pub fn into_lines(self) -> Vec<Vec<char>> {
        self.lines
    }
}

impl Default for Text {
    fn default() -> Text {
        // This is safe because there is exactly one empty line.
        unsafe { Text::from_lines_unchecked(vec![Vec::new()]) }
    }
}

impl FromStr for Text {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Text, ParseError> {
        let mut lines = vec![Vec::new()];
        for ch in s.chars() {
            match ch {
                '\0' => return Err(ParseError),
                '\n' => lines.push(Vec::new()),
                ch => lines.last_mut().unwrap().push(ch),
            }
        }
        // This is safe because:
        // 1. The vector starts out non-empty, and only grows.
        // 2. We never append nil or newline characters to a line.
        Ok(unsafe { Text::from_lines_unchecked(lines) })
    }
}

impl fmt::Display for Text {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for line in &self.lines {
            for ch in line {
                write!(f, "{}", ch)?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

/// Implements the `+=` operator for appending another text to this text.
impl AddAssign for Text {
    #[allow(clippy::suspicious_op_assign_impl)]
    fn add_assign(&mut self, mut other: Text) {
        other
            .lines
            .first_mut()
            .unwrap()
            .splice(..0, self.lines.last_mut().unwrap().iter().cloned());
        self.lines.splice(self.lines.len() - 1.., other.lines);
    }
}

/// An error when creating a text from a vector of lines.
#[derive(Debug)]
pub struct FromLinesError;

impl fmt::Display for FromLinesError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "a line contains either nul or newline characters")
    }
}

impl Error for FromLinesError {}

// An error when parsing a text from a string.
#[derive(Debug)]
pub struct ParseError;

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "string contains nul characters")
    }
}

impl Error for ParseError {}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_take() {
        let mut text =
            Text::from_lines(vec![vec!['a', 'b'], vec!['c', 'd'], vec!['e', 'f']]).unwrap();
        text.take(Size {
            line_count: 1,
            column_count: 1,
        });
        assert_eq!(
            text,
            Text::from_lines(vec![vec!['d'], vec!['e', 'f']]).unwrap()
        );
    }

    #[test]
    fn test_skip() {
        let mut text =
            Text::from_lines(vec![vec!['a', 'b'], vec!['c', 'd'], vec!['e', 'f']]).unwrap();
        text.skip(Size {
            line_count: 1,
            column_count: 1,
        });
        assert_eq!(
            text,
            Text::from_lines(vec![vec!['d'], vec!['e', 'f']]).unwrap()
        );
    }

    #[test]
    fn test_insert_single_line() {
        let mut text = Text::from_lines(vec![vec!['a', 'c']]).unwrap();
        text.insert(
            Position {
                line_index: 0,
                column_index: 1,
            },
            Text::from_lines(vec![vec!['b']]).unwrap(),
        );
        assert_eq!(text, Text::from_lines(vec![vec!['a', 'b', 'c']]).unwrap());
    }

    #[test]
    fn test_insert_multiple_lines() {
        let mut text = Text::from_lines(vec![vec!['a', 'f']]).unwrap();
        text.insert(
            Position {
                line_index: 0,
                column_index: 1,
            },
            Text::from_lines(vec![vec!['b'], vec!['c', 'd'], vec!['e']]).unwrap(),
        );
        assert_eq!(
            text,
            Text::from_lines(vec![vec!['a', 'b'], vec!['c', 'd'], vec!['e', 'f']]).unwrap()
        );
    }

    #[test]
    fn test_delete_single_line() {
        let mut text = Text::from_lines(vec![vec!['a', 'b', 'c']]).unwrap();
        text.delete(
            Position {
                line_index: 0,
                column_index: 1,
            },
            Size {
                line_count: 0,
                column_count: 1,
            },
        );
        assert_eq!(text, Text::from_lines(vec![vec!['a', 'c']]).unwrap())
    }

    #[test]
    fn test_delete_multiple_lines() {
        let mut text =
            Text::from_lines(vec![vec!['a', 'b'], vec!['c', 'd'], vec!['e', 'f']]).unwrap();
        text.delete(
            Position {
                line_index: 0,
                column_index: 1,
            },
            Size {
                line_count: 2,
                column_count: 1,
            },
        );
        assert_eq!(text, Text::from_lines(vec![vec!['a', 'f'],]).unwrap());
    }

    #[test]
    fn test_apply_delta_insert() {
        let mut text = Text::from_lines(vec![vec!['a', 'f']]).unwrap();
        text.apply_delta(Delta::insert(
            Position {
                line_index: 0,
                column_index: 1,
            },
            Text::from_lines(vec![vec!['b'], vec!['c', 'd'], vec!['e']]).unwrap(),
        ));
        assert_eq!(
            text,
            Text::from_lines(vec![vec!['a', 'b'], vec!['c', 'd'], vec!['e', 'f']]).unwrap()
        );
    }

    #[test]
    fn test_apply_delta_delete() {
        let mut text =
            Text::from_lines(vec![vec!['a', 'b'], vec!['c', 'd'], vec!['e', 'f']]).unwrap();
        text.apply_delta(Delta::delete(
            Position {
                line_index: 0,
                column_index: 1,
            },
            Size {
                line_count: 2,
                column_count: 1,
            },
        ));
        assert_eq!(text, Text::from_lines(vec![vec!['a', 'f'],]).unwrap());
    }
}
