use {
    collab_protocol::{Notification, ProjectId, Request, Response, ProjectSnapshot},
    core::{operation::{Operation, FileId}, path::PathBuf, Text},
    std::{
        collections::HashMap,
        sync::{Arc, Mutex},
    },
};

pub fn connect() -> (Sender, Handler) {
    let shared = Arc::new(Mutex::new(Shared {
        projects_by_id: HashMap::new(),
    }));
    (
        Sender {
            shared: shared.clone(),
        },
        Handler { shared },
    )
}

#[derive(Debug)]
pub struct Sender {
    shared: Arc<Mutex<Shared>>,
}

impl Sender {
    pub fn send_request(&self, _request: Request) {
        unimplemented!()
    }
}

#[derive(Debug)]
pub struct Handler {
    shared: Arc<Mutex<Shared>>,
}

impl Handler {
    pub fn handle_response(&self, _response: Response) {
        unimplemented!()
    }

    pub fn handle_notification(&self, _notification: Notification) {
        unimplemented!()
    }
}

#[derive(Debug)]
struct Shared {
    projects_by_id: HashMap<ProjectId, Project>,
}

#[derive(Debug)]
struct Project {
    next_file_id: FileId,
    files_by_id: HashMap<FileId, File>,
    file_ids_by_path: HashMap<PathBuf, FileId>,
    committed_operations: Vec<Operation>,
}


impl From<ProjectSnapshot> for Project {
    fn from(snapshot: ProjectSnapshot)  -> Project {
        for operation in snapshot.committed_operations {
            match operation {
                Operation::CreateFile(path) => {
                    unimplemented!()
                }
                Operation::DeleteFile(path) => {
                    unimplemented!()
                }
                Operation::UpdateFile(file_id, revision_id) => {
                    unimplemented!()
                }
            }
        }
        unimplemented!()
    }
}

#[derive(Debug)]
struct File {
    text: Text
}